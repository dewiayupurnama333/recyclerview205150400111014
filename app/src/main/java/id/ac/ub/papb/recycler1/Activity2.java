package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.TextView;
import android.content.Intent;

public class Activity2 extends AppCompatActivity {
    TextView tvNim, tvNama;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String nim = bundle.getString("nim");
        String nama = bundle.getString("nama");

        tvNim = (TextView) findViewById(R.id.tvNim2);
        tvNama = (TextView) findViewById(R.id.tvNama2);

        tvNim.setText(nim);
        tvNama.setText(nama);
    }
}